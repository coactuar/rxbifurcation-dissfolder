-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 21, 2022 at 01:18 PM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `calciumandcomplications_faculty`
--

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `id` int(50) NOT NULL,
  `FullName` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `city` varchar(200) NOT NULL,
  `hospital` varchar(150) NOT NULL,
  `User Action` varchar(200) NOT NULL,
  `eventname` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`id`, `FullName`, `email`, `city`, `hospital`, `User Action`, `eventname`) VALUES
(1, 'Mandakini', 'mandakini.bhatia@abbott.com', 'Mum', 'Abbott', '09-03-2021 18:40', '10-03-2021 20:29'),
(2, 'Nilesh chawhan', 'nilesh.chawhan@abbott.com', 'NAGPUR', 'Abbott', '10-03-2021 17:37', '10-03-2021 17:38'),
(3, 'Amar Amale', 'amaleamar@gmail.com', 'Nagpur', 'Arneja Heart and Multispeciality Hospital', '10-03-2021 18:19', '10-03-2021 18:20'),
(4, 'Amar Amale', 'amaleamar@gmail.com', 'Nagpur', 'Arneja Heart and Multispeciality Hospital', '10-03-2021 18:33', '10-03-2021 20:30'),
(5, 'DR ANKEET DEDHIA', 'dr.ankeet15@gmail.com', 'MUMBAI', 'HOLY FAMILY HOSPITAL', '10-03-2021 19:00', '10-03-2021 20:29'),
(6, 'dr aj swamy', 'ajayswamy@rediffmail.com', 'bangalore', 'command hospital', '10-03-2021 18:47', '10-03-2021 20:29'),
(7, 'pravin kahale', 'drpravinkahale@yahoo.com', 'mumbai', 'KDAH', '10-03-2021 18:48', '10-03-2021 20:29'),
(8, 'Chetan Shah', 'chetanshah@hotmail.com', 'Mumbai', 'Zynova Heart Hospital', '10-03-2021 18:54', '10-03-2021 20:29'),
(9, 'V Mehan', 'abc@abc', 'Mumbai', 'Lila', '10-03-2021 18:57', '10-03-2021 20:26'),
(10, 'Dr. Sanjeevkumar Kalkekar ', 'drsanjaykalkekar@gmail.com', 'NAVI MUMBAI', 'Apollo Hospitals', '10-03-2021 19:29', '10-03-2021 20:30');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(1, 'neeraj', 'neeraj@coact.co.in', 'nellore', 'bng', NULL, NULL, '2021-03-09 13:54:34', '2021-03-09 13:54:34', '2021-03-09 13:56:04', 1, 'Abbott'),
(2, 'Nishanth', 'nishanth@coact.co.in', 'Bangalore', 'Bangalore', NULL, NULL, '2021-03-09 14:09:06', '2021-03-09 14:09:06', '2021-03-09 14:13:51', 1, 'Abbott'),
(3, 'Nishanth', 'nishanth@coact.co.in', 'Bangalore', 'Bangalore', NULL, NULL, '2021-03-09 17:32:42', '2021-03-09 17:32:42', '2021-03-09 17:33:12', 1, 'Abbott'),
(4, 'Mandakini', 'mandakini.bhatia@abbott.com', 'Mum', 'Abbott', NULL, NULL, '2021-03-09 19:07:52', '2021-03-09 19:07:52', '2021-03-10 17:49:10', 1, 'Abbott'),
(5, 'V', 'abc@abc', 'V', 'V', NULL, NULL, '2021-03-09 19:22:51', '2021-03-09 19:22:51', '2021-03-09 19:23:21', 1, 'Abbott'),
(6, 'V', 'v@abc', 'V', 'V', NULL, NULL, '2021-03-09 19:30:26', '2021-03-09 19:30:26', '2021-03-09 19:30:56', 1, 'Abbott'),
(7, 'dr aj swamy', 'ajayswamy@rediffmail.com', 'bangalore', 'Command hospital', NULL, NULL, '2021-03-09 19:48:58', '2021-03-09 19:48:58', '2021-03-09 19:49:28', 1, 'Abbott'),
(8, 'Nishanth', 'nishanth@coact.co.in', 'Bangalore', 'Bangalore', NULL, NULL, '2021-03-10 15:13:52', '2021-03-10 15:13:52', '2021-03-10 15:14:22', 1, 'Abbott'),
(9, 'Mandakini', 'mandakini.bhatia@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-03-10 17:35:26', '2021-03-10 17:35:26', '2021-03-10 17:49:10', 1, 'Abbott'),
(10, 'Nilesh chawhan', 'nilesh.chawhan@abbott.com', 'NAGPUR', 'Abbott', NULL, NULL, '2021-03-10 17:37:36', '2021-03-10 17:37:36', '2021-03-10 17:38:06', 1, 'Abbott'),
(11, 'Amar Amale', 'amaleamar@gmail.com', 'Nagpur', 'Arneja Heart and Multispeciality Hospital', NULL, NULL, '2021-03-10 18:17:39', '2021-03-10 18:17:39', '2021-03-10 18:18:09', 1, 'Abbott'),
(12, 'DR ANKEET DEDHIA', 'dr.ankeet15@gmail.com', 'MUMBAI', 'HOLY FAMILY HOSPITAL', NULL, NULL, '2021-03-10 18:30:51', '2021-03-10 18:30:51', '2021-03-10 18:31:21', 1, 'Abbott'),
(13, 'Mandakini Bhatia', 'mandakini.bhatia@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-03-10 18:40:17', '2021-03-10 18:40:17', '2021-03-10 18:40:47', 1, 'Abbott'),
(14, 'dr aj swamy', 'ajayswamy@rediffmail.com', 'bangalore', 'command hospital', NULL, NULL, '2021-03-10 18:46:56', '2021-03-10 18:46:56', '2021-03-10 18:47:26', 1, 'Abbott'),
(15, 'pravin kahale', 'drpravinkahale@yahoo.com', 'mumbai', 'KDAH', NULL, NULL, '2021-03-10 18:47:54', '2021-03-10 18:47:54', '2021-03-10 18:48:24', 1, 'Abbott'),
(16, 'Chetan Shah', 'chetanshah@hotmail.com', 'Mumbai', 'Zynova Heart Hospital', NULL, NULL, '2021-03-10 18:53:37', '2021-03-10 18:53:37', '2021-03-10 18:54:07', 1, 'Abbott'),
(17, 'Nilesh chawhan', 'nilesh.chawhan@abbott.com', 'NAGPUR', 'Abbott', NULL, NULL, '2021-03-10 18:56:50', '2021-03-10 18:56:50', '2021-03-10 18:57:20', 1, 'Abbott'),
(18, 'V Mehan', 'abc@abc', 'Mumbai', 'Lila', NULL, NULL, '2021-03-10 18:57:01', '2021-03-10 18:57:01', '2021-03-10 18:57:31', 1, 'Abbott'),
(19, 'Dr. Sanjeevkumar Kalkekar ', 'drsanjaykalkekar@gmail.com', 'NAVI MUMBAI', 'Apollo Hospitals', NULL, NULL, '2021-03-10 19:27:13', '2021-03-10 19:27:13', '2021-03-10 19:27:43', 1, 'Abbott'),
(20, 'Nishanth', 'nishanth@coact.co.in', 'Bangalore', 'Bangalore', NULL, NULL, '2021-03-12 11:18:11', '2021-03-12 11:18:11', '2021-03-12 11:18:41', 1, 'Abbott');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
